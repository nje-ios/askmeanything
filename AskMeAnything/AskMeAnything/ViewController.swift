//
//  ViewController.swift
//  AskMeAnything
//
//  Created by Test User on 2019. 09. 15..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let balls = ["ball1", "ball2", "ball3", "ball4", "ball5", "ball6"]
    
    @IBOutlet weak var ballImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newBallImage()
    }

    @IBAction func grilMeButtonOnTouch(_ sender: UIButton) {
        newBallImage()
    }
    
    func newBallImage() {
        let randomBallIndex = Int(arc4random_uniform(UInt32(balls.count)))
        print("ball index: \(randomBallIndex)")
        ballImageView.image = UIImage(named: balls[randomBallIndex])
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        newBallImage()
    }
}

